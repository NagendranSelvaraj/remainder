
import UserNotifications
import CoreData
import UIKit

class ViewController: UIViewController,UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var table: UITableView!
    
    //let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var models = [MyReminderModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        UNUserNotificationCenter.current().delegate = self
        LocalNotification()
    }
    
    func LocalNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            print(granted)
            if let error = error {
                print(error)
            }
        }
        let content = UNMutableNotificationContent()
        content.title = "MyRemainder"
        content.body = "Remainder Application"
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let requestNotification = "Remainder"
        let request = UNNotificationRequest(identifier: requestNotification, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil {
                print("something went wrong")
            }
        }
    }
   
    
@IBAction func didTapAdd() {
        // show add vc
        guard let vc = storyboard?.instantiateViewController(identifier: "add") as? AddViewController else {
            return
        }
        
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let context = delegate.persistentContainer.viewContext
        
        vc.title = "New Reminder"
        vc.navigationItem.largeTitleDisplayMode = .never
        
        let entity = NSEntityDescription.entity(forEntityName: "MyReminderModel", in: context)!
        
        let  msg = NSManagedObject(entity: entity, insertInto: context) as! MyReminderModel
        
        
        
        vc.completion = { title, body, date in
            DispatchQueue.main.async {
                self.navigationController?.popToRootViewController(animated: true)
                
                msg.date = date
                msg.title = title
                msg.identifer = body
                
                do {
                    try context.save()
                    self.models.append(msg)
                    self.table.reloadData()
                }catch{
                    print(error)
                }
                
                let content = UNMutableNotificationContent()
                content.title = title
                content.sound = .default
                content.body = body
                
                let targetDate = date
    let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year, .month, .day,.hour, .minute],from: targetDate),repeats: false)
                
                let request = UNNotificationRequest(identifier: "Remainder", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
                    if error != nil {
                        print("something went wrong")
                    }
                })
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RemainderCellId", for: indexPath)
        cell.textLabel?.text = models[indexPath.row].title
        let date = models[indexPath.row].date
        
        if date != nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM, dd, YYYY  HH:mm:ss"
            cell.detailTextLabel?.text = formatter.string(from: date!)
            
            cell.textLabel?.font = UIFont(name: "Arial", size: 25)
            cell.detailTextLabel?.font = UIFont(name: "Arial", size: 22)
        }
        return cell
    }
    // Delete in RightSwipe
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            // print("Delete")
            let personremove = self.models[indexPath.row]
            self.models.remove(at: indexPath.row)
            context.delete(personremove)
            do {
                try context.save()
                tableView.reloadData()
            }
            catch {
                
            }
            
        }
        action.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [action])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    /*
     func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
     let delete: UIContextualAction.Handler = { action, view, completion in
     self.removeRemainder(atIndexPath: indexPath)
     completion(true)
     }
     let action = UIContextualAction(style: .destructive, title: "Delete", handler: delete)
     return UISwipeActionsConfiguration(actions: [action])
     } */
    
}

